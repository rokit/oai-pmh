package oaipmh

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

type harvester struct {
	baseURL string
}

func (r request) url() string {
	params := url.Values{}

	params.Set("verb", r.Verb)
	if r.ResumptionToken != "" {
		// if the request has a resumption token, it should be the exclusive parameter (except verb)
		params.Set("resumptionToken", r.ResumptionToken)
	} else {
		if r.MetadataPrefix != "" {
			params.Set("metadataPrefix", r.MetadataPrefix)
		}
		if r.Set != "" {
			params.Set("set", r.Set)
		}
		if r.Identifier != "" {
			params.Set("identifier", r.Identifier)
		}
		if r.From != nil {
			params.Set("from", r.From.String())
		}
		if r.Until != nil {
			params.Set("until", r.Until.String())
		}
	}
	return r.BaseURL + "?" + params.Encode()
}

func (r *request) get() (*response, error) {
	// fmt.Println(r.URL())
	resp, err := http.Get(r.url())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var response response
	if err = xml.Unmarshal(b, &response); err != nil {
		fmt.Println("error unmarshaling to OAI-PMH element:", string(b))
		return nil, err
	}
	if response.Error != nil {
		return nil, errors.New(response.Error.Body)
	}
	return &response, nil
}

func (h harvester) Identify() (*identify, error) {
	response, err := h.newRequest().withVerb("Identify").get()
	if err != nil {
		return nil, err
	}
	return response.Identify, nil
}

func (h harvester) ListRecords(from *time.Time, until *time.Time, metadataPrefix string, set string, callback func([]Record)) error {
	return h.harvest(h.newRequest().
		withVerb("ListRecords").
		withMetadataPrefix(metadataPrefix).
		withSet(set).
		withFrom(from).
		withUntil(until),
		func(r *response) {
			if r.ListRecords != nil {
				callback(r.ListRecords.Records)
			}
		})
}

func (h harvester) GetRecord(identifier string, metadataPrefix string) (*Record, error) {
	response, err := h.newRequest().withVerb("GetRecord").withIdentifier(&identifier).withMetadataPrefix(metadataPrefix).get()
	if err != nil {
		return nil, err
	}
	if response.GetRecord != nil {
		return &response.GetRecord.Record, nil
	}
	return nil, errors.New("no record")
}

func (h harvester) ListSets(callback func([]Set)) error {
	return h.harvest(h.newRequest().withVerb("ListSets"),
		func(r *response) {
			if r.ListSets != nil {
				callback(r.ListSets.Sets)
			}
		})
}

func (h harvester) ListIdentifiers(from *time.Time, until *time.Time, metadataPrefix string, set string, callback func([]Header)) error {
	return h.harvest(h.newRequest().
		withVerb("ListIdentifiers").
		withMetadataPrefix(metadataPrefix).
		withSet(set).
		withFrom(from).
		withUntil(until),
		func(r *response) {
			if r.ListIdentifiers != nil {
				callback(r.ListIdentifiers.Headers)
			}
		})
}

func (h harvester) ListMetadataFormats(identifier *string) ([]MetadataFormat, error) {
	response, err := h.newRequest().withVerb("ListMetadataFormats").withIdentifier(identifier).get()
	if err != nil {
		return nil, err
	}
	if response.ListMetadataFormats != nil {
		return response.ListMetadataFormats.MetadataFormats, nil
	}
	return nil, errors.New("no metadata formats")
}

func (h harvester) newRequest() *request {
	return &request{
		BaseURL: h.baseURL,
	}
}

func (h harvester) harvest(r *request, callback func(*response)) error {
	response, err := r.get()
	if err != nil {
		return err
	}
	callback(response)

	// reset possible previous resumption token
	r.ResumptionToken = ""

	// check if this was an incomplete list request (i.e. if response contains a resumption token)
	if response.ListIdentifiers != nil && response.ListIdentifiers.ResumptionToken != nil {
		r.ResumptionToken = response.ListIdentifiers.ResumptionToken.Body
	} else if response.ListRecords != nil && response.ListRecords.ResumptionToken != nil {
		r.ResumptionToken = response.ListRecords.ResumptionToken.Body
	} else if response.ListSets != nil && response.ListSets.ResumptionToken != nil {
		r.ResumptionToken = response.ListSets.ResumptionToken.Body
	}
	// if we have a resumption token, make a follow-up request
	if r.ResumptionToken != "" {
		return h.harvest(r, callback)
	}
	return nil
}

func NewHarvester(baseURL string) harvester {
	return harvester{
		baseURL: baseURL,
	}
}
