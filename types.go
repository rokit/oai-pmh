package oaipmh

import (
	"encoding/xml"
	"time"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/mets"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/mods"
	oaidc "gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/oai-dc"
)

const errBadArgument = "badArgument"
const errBadVerb = "badVerb"
const errCannotDisseminateFormat = "cannotDisseminateFormat"
const errIdDoesNotExist = "idDoesNotExist"
const errNoMetadataFormats = "noMetadataFormats"
const errNoRecordsMatch = "noRecordsMatch"
const errBadResumptionToken = "badResumptionToken"
const errNoSetHierarchy = "noSetHierarchy"

const RecordDeletionNo recordDeletionStrategy = "no"
const RecordDeletionTransient recordDeletionStrategy = "transient"
const RecordDeletionPersistent recordDeletionStrategy = "persistent"

const DatestampGranularityDate datestampGranularity = "YYYY-MM-DD"
const DatestampGranularityDateTime datestampGranularity = "YYYY-MM-DDThh:mm:ssZ"

type recordDeletionStrategy string
type datestampGranularity string

type oaipmhError struct {
	XMLName xml.Name `xml:"error"`
	Body    string   `xml:",chardata"`
	Code    string   `xml:"code,attr"`
}

type identify struct {
	RepositoryName    string                    `xml:"repositoryName"`
	BaseURL           string                    `xml:"baseURL"`
	ProtocolVersion   string                    `xml:"protocolVersion"`
	AdminEmail        []string                  `xml:"adminEmail"`
	EarliestDatestamp metadataformats.DateStamp `xml:"earliestDatestamp"`
	DeletedRecord     recordDeletionStrategy    `xml:"deletedRecord"` // "no", "persistent", "transient"
	Granularity       datestampGranularity      `xml:"granularity"`   // "YYYY-MM-DD", "YYYY-MM-DDThh:mm:ssZ"
	Compressions      []string                  `xml:"compression,omitempty"`
	Descriptions      []Description             `xml:"description,omitempty"`
}

func (i *identify) AddDescription(description any) *identify {
	i.Descriptions = append(i.Descriptions, Description{Body: description})
	return i
}

type listMetadataFormats struct {
	MetadataFormats []MetadataFormat `xml:"metadataFormat"`
}

type listSets struct {
	Sets            []Set            `xml:"set"`
	ResumptionToken *ResumptionToken `xml:",omitempty"`
}

type getRecord struct {
	Record Record
}

type listRecords struct {
	Records         []Record         `xml:"record"`
	ResumptionToken *ResumptionToken `xml:",omitempty"`
}

type listIdentifiers struct {
	Headers         []Header         `xml:"header"`
	ResumptionToken *ResumptionToken `xml:",omitempty"`
}

type Record struct {
	XMLName  xml.Name `xml:"record"`
	Header   Header
	Metadata Metadata `xml:",omitempty"`
	Abouts   []About  `xml:"about,omitempty"`
}

type Header struct {
	XMLName    xml.Name                  `xml:"header"`
	Identifier string                    `xml:"identifier"`
	Datestamp  metadataformats.DateStamp `xml:"datestamp"`
	SetSpecs   []string                  `xml:"setSpec,omitempty"`
	Status     string                    `xml:"status,attr,omitempty"` // "deleted"
}

type ResumptionToken struct {
	XMLName          xml.Name   `xml:"resumptionToken"`
	Body             string     `xml:",chardata"`
	ExpirationDate   *time.Time `xml:"expirationDate,attr,omitempty"`
	CompleteListSize *uint      `xml:"completeListSize,attr,omitempty"`
	Cursor           *uint      `xml:"cursor,attr,omitempty"`
}

type Metadata struct {
	XMLName xml.Name `xml:"metadata"`
	Body    any      `xml:",innerxml"`
}

type MetadataFormat struct {
	XMLName           xml.Name `xml:"metadataFormat"`
	MetadataPrefix    string   `xml:"metadataPrefix"`
	Schema            string   `xml:"schema"`
	MetadataNamespace string   `xml:"metadataNamespace"`
}

var OaiDcMetadataFormat = MetadataFormat{
	MetadataPrefix:    oaidc.PREFIX,
	MetadataNamespace: oaidc.NAMESPACE,
	Schema:            oaidc.SCHEMA_LINK,
}

var MetsMetadataFormat = MetadataFormat{
	MetadataPrefix:    mets.PREFIX,
	MetadataNamespace: mets.NAMESPACE,
	Schema:            mets.SCHEMA_LINK,
}

var ModsMetadataFormat = MetadataFormat{
	MetadataPrefix:    mods.PREFIX,
	MetadataNamespace: mods.NAMESPACE,
	Schema:            mods.SCHEMA_LINK,
}

type About struct {
	XMLName xml.Name `xml:"about"`
	Body    []byte   `xml:",innerxml"`
}

type Description struct {
	XMLName xml.Name `xml:"description"`
	Body    any      `xml:",innerxml"`
}

type Set struct {
	XMLName         xml.Name      `xml:"set"`
	SetSpec         string        `xml:"setSpec"`
	SetName         string        `xml:"setName"`
	SetDescriptions []Description `xml:"setDescription,omitempty"`
}

type langString struct {
	Body string `xml:",chardata"`
	Lang string `xml:"xml:lang,omitempty"`
}

func NewLangString(body string, lang *string) langString {
	result := langString{
		Body: body,
	}
	if lang != nil {
		result.Lang = *lang
	}
	return result
}
