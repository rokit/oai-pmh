module gitlab.ulb.tu-darmstadt.de/rokit/oai-pmh

go 1.22

toolchain go1.22.0

require (
	github.com/flytam/filenamify v1.2.0
	github.com/knakk/rdf v0.0.0-20190304171630-8521bf4c5042
	github.com/knakk/sparql v0.0.0-20240119140508-255b851aa040
	gitlab.ulb.tu-darmstadt.de/rokit/metadataformats v0.0.3
)

require github.com/knakk/digest v0.0.0-20160404164910-fd45becddc49 // indirect
