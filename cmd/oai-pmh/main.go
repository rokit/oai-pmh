package main

import (
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/flytam/filenamify"
	oaipmh "gitlab.ulb.tu-darmstadt.de/rokit/oai-pmh"
)

const dateFormat = "2006-01-02"

var endpoint string
var targetDir string
var from string
var until string
var set string
var identifier string
var metadataPrefix string

var parsedFrom *time.Time
var parsedUntil *time.Time

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Missing required command [identify, records, record, identifiers, identifiers-records, sets, formats]")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "identify":
		fs := flag.NewFlagSet("identify", flag.ExitOnError)
		initCommand(fs)
		identify()
	case "records":
		fs := flag.NewFlagSet("records", flag.ExitOnError)
		fs.StringVar(&metadataPrefix, "prefix", "", "metadata prefix")
		fs.StringVar(&set, "set", "", "metadata set name [optional]")
		fs.StringVar(&from, "from", "", "date in format yyyy-mm-dd from which to list records [optional]")
		fs.StringVar(&until, "until", "", "date in format yyyy-mm-dd up to which to list records [optional]")
		initCommand(fs, &metadataPrefix)
		parsedFrom = parseDate(from)
		parsedUntil = parseDate(until)
		listRecords()
	case "record":
		fs := flag.NewFlagSet("record", flag.ExitOnError)
		fs.StringVar(&identifier, "identifier", "", "identifier of the item for which available metadata formats are being requested")
		fs.StringVar(&metadataPrefix, "prefix", "", "metadata prefix")
		initCommand(fs, &identifier, &metadataPrefix)
		getRecord()
	case "identifiers":
		fs := flag.NewFlagSet("identifiers", flag.ExitOnError)
		fs.StringVar(&metadataPrefix, "prefix", "", "metadata prefix")
		fs.StringVar(&set, "set", "", "metadata set name [optional]")
		fs.StringVar(&from, "from", "", "date in format yyyy-mm-dd from which to list identifiers [optional]")
		fs.StringVar(&until, "until", "", "date in format yyyy-mm-dd up to which to list identifiers [optional]")
		initCommand(fs, &metadataPrefix)
		parsedFrom = parseDate(from)
		parsedUntil = parseDate(until)
		listIdentifiers()
	case "identifiers-records":
		fs := flag.NewFlagSet("identifiers-records", flag.ExitOnError)
		fs.StringVar(&metadataPrefix, "prefix", "", "metadata prefix")
		fs.StringVar(&set, "set", "", "metadata set name [optional]")
		fs.StringVar(&from, "from", "", "date in format yyyy-mm-dd from which to list identifiers [optional]")
		fs.StringVar(&until, "until", "", "date in format yyyy-mm-dd up to which to list identifiers [optional]")
		initCommand(fs, &metadataPrefix)
		parsedFrom = parseDate(from)
		parsedUntil = parseDate(until)
		listIdentifiersRecords()
	case "sets":
		fs := flag.NewFlagSet("sets", flag.ExitOnError)
		initCommand(fs)
		listSets()
	case "formats":
		fs := flag.NewFlagSet("formats", flag.ExitOnError)
		fs.StringVar(&identifier, "identifier", "", "identifier of the item for which available metadata formats are being requested [optional]")
		initCommand(fs)
		listFormats()

	default:
		fmt.Printf("unknown command '%s'. Valid commands are [identify, records, record, identifiers, sets, formats]\n", os.Args[1])
		os.Exit(1)
	}
}

func initCommand(fs *flag.FlagSet, required ...*string) {
	fs.StringVar(&endpoint, "endpoint", "", "OAI-PMH endpoint")
	fs.StringVar(&targetDir, "target", "", "directory to store OAI-PMH data in. If not specified, print to stdout [optional]")

	fs.Parse(os.Args[2:])
	if endpoint == "" {
		exitWithMessage("no endpoint specified", fs)
	}
	for _, r := range required {
		if *r == "" {
			exitWithMessage("missing required flag", fs)
		}
	}
	if targetDir != "" {
		if _, err := os.Stat(targetDir); errors.Is(err, os.ErrNotExist) {
			// targetDir does not exist, so create it
			if err := os.Mkdir(targetDir, os.ModePerm); err != nil {
				panic(err)
			}
		}
	}
}

func exitWithMessage(message string, fs *flag.FlagSet) {
	fmt.Println(message)
	fs.Usage()
	os.Exit(1)
}

func parseDate(d string) *time.Time {
	if d == "" {
		return nil
	}
	parsed, err := time.Parse(dateFormat, d)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return &parsed
}

func initDateFromTargetDir() *time.Time {
	var dateInTargetDir *time.Time
	dir, err := os.Open(targetDir)
	if err != nil {
		panic(err)
	}
	defer dir.Close()

	var filenames []string
	for ; err == nil; filenames, err = dir.Readdirnames(1000) {
		for _, filename := range filenames {
			var record oaipmh.Record
			file, err := os.Open(filepath.Join(targetDir, filename))
			if err != nil {
				panic(err)
			}
			defer file.Close()
			if err := xml.NewDecoder(file).Decode(&record); err != nil {
				panic(err)
			}
			file.Close()
			recordTime := record.Header.Datestamp.Time
			if dateInTargetDir == nil || recordTime.After(*dateInTargetDir) {
				dateInTargetDir = &recordTime
			}
		}
	}
	if !errors.Is(err, io.EOF) {
		panic(err)
	}
	return dateInTargetDir
}

func identify() {
	fmt.Printf("identifying %s\n", endpoint)

	identify, err := oaipmh.NewHarvester(endpoint).Identify()
	if err != nil {
		panic(err)
	}
	writeResult("identify", identify)

}

func listRecords() {
	if targetDir != "" && parsedFrom == nil {
		parsedFrom = initDateFromTargetDir()
	}
	fmt.Println("listing records on ", endpoint, "from", parsedFrom, "until", parsedUntil)

	err := oaipmh.NewHarvester(endpoint).ListRecords(parsedFrom, parsedUntil, metadataPrefix, set, func(records []oaipmh.Record) {
		for _, record := range records {
			writeResult(record.Header.Identifier, record)
		}
	})
	if err != nil {
		panic(err)
	}
}

func listIdentifiers() {
	if targetDir != "" && parsedFrom == nil {
		parsedFrom = initDateFromTargetDir()
	}
	fmt.Println("listing identifiers on ", endpoint, "from", parsedFrom, "until", parsedUntil)

	err := oaipmh.NewHarvester(endpoint).ListIdentifiers(parsedFrom, parsedUntil, metadataPrefix, set, func(headers []oaipmh.Header) {
		for _, header := range headers {
			writeResult(header.Identifier, header)
		}
	})
	if err != nil {
		panic(err)
	}
}

func listIdentifiersRecords() {
	if targetDir != "" && parsedFrom == nil {
		parsedFrom = initDateFromTargetDir()
	}
	fmt.Println("listing identifiers and harvesting records on ", endpoint, "from", parsedFrom, "until", parsedUntil)

	harvester := oaipmh.NewHarvester(endpoint)
	err := harvester.ListIdentifiers(parsedFrom, parsedUntil, metadataPrefix, set, func(headers []oaipmh.Header) {
		for _, header := range headers {
			record, err := harvester.GetRecord(header.Identifier, metadataPrefix)
			if err != nil {
				fmt.Println(header)
				panic(err)
			}
			writeResult(record.Header.Identifier, record)
		}
	})
	if err != nil {
		panic(err)
	}
}

func listSets() {
	fmt.Printf("listing sets on %s\n", endpoint)
	err := oaipmh.NewHarvester(endpoint).ListSets(func(sets []oaipmh.Set) {
		for _, set := range sets {
			writeResult(set.SetSpec, set)
		}
	})
	if err != nil {
		panic(err)
	}
}

func listFormats() {
	if identifier == "" {
		fmt.Printf("listing all formats on %s\n", endpoint)
	} else {
		fmt.Printf("listing formats of %s on %s\n", identifier, endpoint)

	}
	formats, err := oaipmh.NewHarvester(endpoint).ListMetadataFormats(&identifier)
	if err != nil {
		panic(err)
	}
	for _, format := range formats {
		writeResult(format.MetadataPrefix, format)
	}
}

func getRecord() {
	fmt.Printf("getting record %s on %s\n", identifier, endpoint)

	record, err := oaipmh.NewHarvester(endpoint).GetRecord(identifier, metadataPrefix)
	if err != nil {
		panic(err)
	}
	writeResult(record.Header.Identifier, record)
}

func writeResult(filename string, result any) {
	data, err := xml.Marshal(result)
	if err != nil {
		panic(err)
	}
	if targetDir == "" {
		fmt.Println(string(data))
	} else {
		sanitized, err := filenamify.FilenamifyV2(filename, func(options *filenamify.Options) {
			options.MaxLength = 255
			options.Replacement = "_"
		})
		if err != nil {
			panic(err)
		}
		if err := os.WriteFile(path.Join(targetDir, sanitized), data, 0644); err != nil {
			panic(err)
		}
	}
}
