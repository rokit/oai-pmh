package main

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/knakk/rdf"
	"github.com/knakk/sparql"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/mets"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/mods"
	oaidc "gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/oai-dc"
	oaipmh "gitlab.ulb.tu-darmstadt.de/rokit/oai-pmh"
)

var sparqlRepo *sparql.Repo
var sparqlPageLimit uint = 10

type testRepository struct{}

func loadGraph(id string) ([]rdf.Triple, error) {
	triples, err := sparqlRepo.Construct(fmt.Sprintf("CONSTRUCT { ?s ?p ?o } WHERE { <%s> (<>|!<>)* ?s . ?s ?p ?o }", id))
	if err != nil {
		return nil, err
	}
	if len(triples) == 0 {
		return nil, errors.New("not found")
	}
	return triples, nil
}

func minIssuedDate() time.Time {
	defaultDate := time.Time{}
	results, err := sparqlRepo.Query("SELECT (MIN(?date) as ?min_date) WHERE { ?s <http://purl.org/dc/terms/issued> ?date }")
	if err != nil {
		log.Println(err)
		return defaultDate
	}
	if len(results.Solutions()) == 1 {
		minDate, err := time.Parse(time.RFC3339, fmt.Sprint(results.Solutions()[0]["min_date"]))
		if err != nil {
			log.Println(err)
			return defaultDate
		}
		return minDate

	} else {
		log.Println("repository has no triple with predicate <http://purl.org/dc/terms/issued>")
		return defaultDate
	}
}

func tripleValues(subj string, pred string, triples []rdf.Triple) []string {
	values := []string{}
	for _, triple := range triples {
		if triple.Subj.String() == subj && triple.Pred.String() == pred {
			values = append(values, fmt.Sprint(triple.Obj))
		}
	}
	return values
}

func tripleValueTerms(subj string, pred string, triples []rdf.Triple) []rdf.Term {
	values := []rdf.Term{}
	for _, triple := range triples {
		if triple.Subj.String() == subj && triple.Pred.String() == pred {
			values = append(values, triple.Obj)
		}
	}
	return values
}

func tripleValue(subj string, pred string, triples []rdf.Triple) string {
	for _, triple := range triples {
		if triple.Subj.String() == subj && triple.Pred.String() == pred {
			return triple.Obj.String()
		}
	}
	return ""
}

func (testRepository) ListIdentifiers(metadataPrefix string, set *string, from *time.Time, until *time.Time, offset uint) ([]oaipmh.Header, *uint, error) {
	filter := ""
	if from != nil {
		filter += fmt.Sprintf(`FILTER (?date >= "%s"^^xsd:dateTime) `, from.Format(time.RFC3339))
	}
	if until != nil {
		filter += fmt.Sprintf(`FILTER (?date <= "%s"^^xsd:dateTime) `, until.Format(time.RFC3339))
	}
	query := fmt.Sprintf(`PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		SELECT DISTINCT ?s ?date
		WHERE { ?s a <http://www.w3.org/ns/dcat#Dataset>; <http://purl.org/dc/terms/modified> ?date . %s }
		ORDER BY (?s)
		LIMIT %d
		OFFSET %d`, filter, sparqlPageLimit, offset)
	fmt.Println(query)
	results, err := sparqlRepo.Query(query)
	if err != nil {
		return nil, nil, err
	}
	headers := make([]oaipmh.Header, 0)
	for _, hit := range results.Solutions() {
		date, err := time.Parse(time.RFC3339, fmt.Sprint(hit["date"]))
		if err != nil {
			return nil, nil, err
		}
		headers = append(headers, oaipmh.Header{
			Identifier: fmt.Sprint(hit["s"]),
			Datestamp:  metadataformats.DateStamp{Time: date},
		})
	}
	return headers, nil, nil
}

func (testRepository) ListMetadataFormats(identifier *string) []oaipmh.MetadataFormat {
	return []oaipmh.MetadataFormat{oaipmh.OaiDcMetadataFormat, oaipmh.ModsMetadataFormat, oaipmh.MetsMetadataFormat}
}

func (repo testRepository) ListRecords(metadataPrefix string, set *string, from *time.Time, until *time.Time, offset uint) ([]oaipmh.Record, *uint, error) {
	headers, completeListSize, err := repo.ListIdentifiers(metadataPrefix, set, from, until, offset)
	if err != nil {
		return nil, nil, err
	}
	records := make([]oaipmh.Record, 0)
	for _, header := range headers {
		record, err := repo.GetRecord(header.Identifier, metadataPrefix)
		if err != nil {
			return nil, nil, err
		}
		if record != nil {
			records = append(records, *record)
		}
	}
	return records, completeListSize, nil
}

func (testRepository) GetRecord(identifier string, metadataPrefix string) (*oaipmh.Record, error) {
	triples, err := loadGraph(identifier)
	if err != nil {
		return nil, err
	}
	var md any
	if metadataPrefix == "oai_dc" {
		md = createOaiDcMetadata(identifier, triples)
	} else if metadataPrefix == "mods" {
		md = createModsMetadata(identifier, triples)
	} else if metadataPrefix == "mets" {
		md = createMetsMetadata(identifier, triples)
	} // else {
	// NOP, because GetRecord() will only be called with supported metadata prefixes
	//}
	lastModifiedDates := tripleValues(identifier, "http://purl.org/dc/terms/modified", triples)
	if len(lastModifiedDates) != 1 {
		return nil, fmt.Errorf("record %s does not have exactly one http://purl.org/dc/terms/modified relation", identifier)
	}
	lastModified, err := time.Parse(time.RFC3339, lastModifiedDates[0])
	if err != nil {
		return nil, err
	}
	record := oaipmh.Record{
		Header: oaipmh.Header{
			Identifier: identifier,
			Datestamp:  metadataformats.DateStamp{Time: lastModified},
		},
		Metadata: oaipmh.Metadata{Body: md},
	}
	return &record, nil
}

func (testRepository) ListSets(offset uint) ([]oaipmh.Set, *uint, error) {
	return []oaipmh.Set{}, nil, nil
}

func (testRepository) Describe() oaipmh.RepositoryInfo {
	info := oaipmh.RepositoryInfo{
		Name:                   "NFDI4Ing ing.est",
		AdminEmail:             "ingest@nfdi4ing.de",
		EarliestDatestamp:      minIssuedDate(),
		RecordDeletionStrategy: oaipmh.RecordDeletionPersistent,
		DatestampGranularity:   oaipmh.DatestampGranularityDateTime,
		MaxPageSize:            sparqlPageLimit,
		Descriptions:           []any{"This is a test repo"},
		// BaseURL:                "http://localhost:3000/",
	}
	return info
}

func mD5Hash(text string, prefix string) string {
	hash := md5.Sum([]byte(text))
	return prefix + hex.EncodeToString(hash[:])
}

func createMetsMetadata(identifier string, triples []rdf.Triple) *mets.Metadata {
	modsMeta := createModsMetadata(identifier, triples)
	hash := mD5Hash(identifier, "_")
	dmdId := fmt.Sprintf("dmd%s", hash)
	metsMeta := mets.NewMetadata().WithId(hash).WithType("dataset")
	hdr := metsMeta.SetMetsHdr()
	for _, creatorSubject := range tripleValues(identifier, "http://purl.org/dc/terms/creator", triples) {
		hdr.AddAgent(tripleValue(creatorSubject, "http://xmlns.com/foaf/0.1/name", triples)).WithRole(mets.ROLE_CREATOR)
	}

	metsMeta.AddDmdSec(dmdId).SetXmlMdWrap(mets.MD_MODS, modsMeta)
	metsMeta.AddStructMap("LOGICAL").AddDiv().WithAdmId(dmdId)
	fileGroup := metsMeta.SetFileSec().AddFileGrp("ORIGINAL")
	for _, fileId := range tripleValues(identifier, "http://purl.org/dc/terms/hasPart", triples) {
		file := fileGroup.AddFile(mD5Hash(fileId, "f_"))
		file.AddFLocat(mets.LOC_URL).WithHref(fileId)
		file.WithMimeType(tripleValue(fileId, "http://www.w3.org/ns/dcat#mediaType", triples))
		if size, err := strconv.Atoi(tripleValue(fileId, "http://www.w3.org/ns/dcat#byteSize", triples)); err == nil {
			file.WithSize(int64(size))
		}
		for _, previewFileId := range tripleValues(fileId, "http://purl.org/rokit/elements/1.1/hasRepresentation", triples) {
			previewFile := file.AddFile(mD5Hash(previewFileId, "f_")).WithUse("PREVIEW")
			previewFile.AddFLocat(mets.LOC_URL).WithHref(previewFileId)
			previewFile.WithMimeType(tripleValue(previewFileId, "http://www.w3.org/ns/dcat#mediaType", triples))
			if size, err := strconv.Atoi(tripleValue(previewFileId, "http://www.w3.org/ns/dcat#byteSize", triples)); err == nil {
				previewFile.WithSize(int64(size))
			}
		}
	}
	return metsMeta
}

func createModsMetadata(identifier string, triples []rdf.Triple) *mods.Metadata {
	modsMeta := mods.NewMetadata()
	modsMeta.AddIdentifier(identifier, "uri")
	for _, id := range tripleValues(identifier, "http://purl.org/dc/terms/identifier", triples) {
		modsMeta.AddIdentifier(id, "uri")
	}
	for _, title := range tripleValueTerms(identifier, "http://purl.org/dc/terms/title", triples) {
		ti := modsMeta.AddTitleInfo().AddTitle(title.String())
		if s, ok := title.(rdf.Literal); ok && s.Lang() != "" {
			ti.Lang = s.Lang()
		}
	}
	for _, license := range tripleValueTerms(identifier, "http://purl.org/dc/terms/license", triples) {
		modsMeta.AddAccessCondition(license.String(), "useAndReproduction")
	}
	oi := modsMeta.AddOriginInfo()
	for _, issued := range tripleValues(identifier, "http://purl.org/dc/terms/issued", triples) {
		oi.AddDateIssued(issued, mods.DATE_ENCODING_ISO_8601, "", "")
	}

	for _, creatorSubject := range tripleValues(identifier, "http://purl.org/dc/terms/creator", triples) {
		modsMeta.AddName().AddNamePart(tripleValue(creatorSubject, "http://xmlns.com/foaf/0.1/name", triples), "")
	}
	return modsMeta
}

func createOaiDcMetadata(identifier string, triples []rdf.Triple) *oaidc.Metadata {
	return &oaidc.Metadata{
		Titles:       tripleValues(identifier, "http://purl.org/dc/terms/title", triples),
		Descriptions: tripleValues(identifier, "http://purl.org/dc/terms/description", triples),
		Rights:       tripleValues(identifier, "http://purl.org/dc/terms/license", triples),
		Dates:        tripleValues(identifier, "http://purl.org/dc/terms/issued", triples),
		Identifiers:  tripleValues(identifier, "http://purl.org/dc/terms/identifier", triples),
		Publishers:   tripleValues(identifier, "http://purl.org/dc/terms/publisher", triples),
		Types:        tripleValues(identifier, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", triples),
	}

}

func main() {
	var err error
	if sparqlRepo, err = sparql.NewRepo("https://rokit.0pq.de/api/v1/sparql",
		sparql.Timeout(time.Second*60),
	); err != nil {
		panic(err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", oaipmh.NewRepositoryHandleFunc(testRepository{}))
	if err := http.ListenAndServe(":3000", mux); err != nil {
		panic(err.Error())
	}

}
