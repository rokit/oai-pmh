package oaipmh

import (
	"encoding/base64"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
)

type simpleResumptionToken struct {
	ResumptionToken
	offset         uint
	metadataPrefix string
	set            *string
	from           *time.Time
	until          *time.Time
}

func (t *simpleResumptionToken) Decode(content string) error {
	decoded, err := base64.URLEncoding.DecodeString(content)
	if err != nil {
		return err
	}
	values, err := url.ParseQuery(string(decoded))
	if err != nil {
		return err
	}
	offset, err := strconv.Atoi(values.Get("offset"))
	if err != nil {
		return err
	}
	t.offset = uint(offset)
	set := values.Get("set")
	if set != "" {
		t.set = &set
	}
	t.metadataPrefix = values.Get("metadataPrefix")
	from := values.Get("from")
	if from != "" {
		parsedFrom, _, err := metadataformats.ParseTime(from)
		if err != nil {
			return err
		}
		t.from = parsedFrom
	}
	until := values.Get("until")
	if until != "" {
		parsedUntil, _, err := metadataformats.ParseTime(until)
		if err != nil {
			return err
		}
		t.until = parsedUntil
	}
	return nil
}

func (t *simpleResumptionToken) Encode() error {
	values := url.Values{}
	values.Set("offset", fmt.Sprint(t.offset))
	values.Set("metadataPrefix", t.metadataPrefix)
	if t.set != nil {
		values.Set("set", *t.set)
	}
	if t.from != nil {
		values.Set("from", t.from.UTC().Format(time.RFC3339))
	}
	if t.until != nil {
		values.Set("until", t.until.UTC().Format(time.RFC3339))
	}
	t.Body = base64.URLEncoding.EncodeToString([]byte(values.Encode()))
	// t.Cursor = &t.offset
	return nil
}
