package oaipmh

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
)

type Repository interface {
	ListIdentifiers(metadataPrefix string, set *string, from *time.Time, until *time.Time, offset uint) (identifiers []Header, completeListSize *uint, err error)
	ListRecords(metadataPrefix string, set *string, from *time.Time, until *time.Time, offset uint) (records []Record, completeListSize *uint, err error)
	ListSets(offset uint) (sets []Set, completeListSize *uint, err error)
	ListMetadataFormats(identifier *string) (formats []MetadataFormat)
	GetRecord(identifier string, metadataPrefix string) (record *Record, err error)
	Describe() RepositoryInfo
}

type RepositoryInfo struct {
	Name                   string
	BaseURL                *string
	AdminEmail             string
	EarliestDatestamp      time.Time
	RecordDeletionStrategy recordDeletionStrategy
	DatestampGranularity   datestampGranularity
	MaxPageSize            uint
	Descriptions           []any
}

func NewRepositoryHandleFunc(repo Repository) func(w http.ResponseWriter, r *http.Request) {
	info := repo.Describe()

	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				log.Println(r)
				w.WriteHeader(500)
				w.Write([]byte(fmt.Sprint(r)))
			}
		}()

		var resp = newResponse(baseURL(info, r))
		if err := r.ParseForm(); err != nil {
			sendError(errBadArgument, "the request could not be parsed", resp, w)
			return
		}

		var offset uint = 0
		verb := r.FormValue("verb")
		var identifier *string
		metadataPrefix := r.FormValue("metadataPrefix")
		var set *string
		setParam := r.FormValue("set")
		if setParam != "" {
			set = &setParam
		}
		identifierParam := r.FormValue("identifier")
		if identifierParam != "" {
			identifier = &identifierParam
		}
		var from *time.Time
		var until *time.Time
		resumptionToken := &simpleResumptionToken{}

		var err error
		var fromDateTimeGranularity *bool
		var untilDateTimeGranularity *bool
		if r.FormValue("from") != "" {
			var granularity bool
			from, granularity, err = metadataformats.ParseTime(r.FormValue("from"))
			if err != nil {
				sendError(errBadArgument, "failed parsing parameter 'from'", resp, w)
				return
			}
			fromDateTimeGranularity = &granularity
		}
		if r.FormValue("until") != "" {
			var granularity bool
			until, granularity, err = metadataformats.ParseTime(r.FormValue("until"))
			if err != nil {
				sendError(errBadArgument, "failed parsing parameter 'until'", resp, w)
				return
			}
			untilDateTimeGranularity = &granularity
		}
		if fromDateTimeGranularity != nil && untilDateTimeGranularity != nil && *fromDateTimeGranularity != *untilDateTimeGranularity {
			sendError(errBadArgument, "parameters 'from' and 'until' have different granularities", resp, w)
			return
		}

		token := r.FormValue("resumptionToken")
		if token != "" {
			if metadataPrefix != "" || identifier != nil || set != nil || from != nil || until != nil {
				// only "verb" and "resumptionToken" allowed
				sendError(errBadArgument, "too many parameters. only 'verb' and 'resumptionToken' are allowed.", resp, w)
				return
			}
			if err := resumptionToken.Decode(token); err != nil {
				sendError(errBadResumptionToken, escapeXML(err.Error()), resp, w)
				return
			}
			metadataPrefix = resumptionToken.metadataPrefix
			from = resumptionToken.from
			until = resumptionToken.until
			set = resumptionToken.set
			offset = resumptionToken.offset
		} else {
			resumptionToken.metadataPrefix = metadataPrefix
			resumptionToken.set = set
			resumptionToken.from = from
			resumptionToken.until = until
		}

		switch verb {
		case "ListRecords":
			if metadataPrefix == "" {
				sendError(errBadArgument, "missing required parameter 'metadataPrefix'", resp, w)
				return
			}
			if err = checkMetadataPrefix(metadataPrefix, repo, nil); err != nil {
				sendError(errCannotDisseminateFormat, "this repository does not support given metadata format", resp, w)
				return
			}
			records, completeListSize, err := repo.ListRecords(metadataPrefix, set, from, until, offset)
			if err != nil {
				panic(err)
			}
			if len(records) == 0 {
				sendError(errNoRecordsMatch, "the request produced no results", resp, w)
				return
			}
			resp.ListRecords = &listRecords{Records: records}
			if uint(len(records)) == info.MaxPageSize {
				resumptionToken.offset += info.MaxPageSize
				if err := resumptionToken.Encode(); err != nil {
					panic(err)
				}
				resumptionToken.CompleteListSize = completeListSize
				resp.ListRecords.ResumptionToken = &resumptionToken.ResumptionToken
			}
			resp.setupSchemaLocations(metadataPrefix)
		case "Identify":
			if token != "" || metadataPrefix != "" || identifier != nil || set != nil || from != nil || until != nil {
				sendError(errBadArgument, "too many parameters. only 'verb' is allowed.", resp, w)
				return
			}
			resp.Identify = &identify{
				RepositoryName:    info.Name,
				BaseURL:           baseURL(info, r),
				ProtocolVersion:   "2.0",
				DeletedRecord:     info.RecordDeletionStrategy,
				Granularity:       info.DatestampGranularity,
				AdminEmail:        []string{info.AdminEmail},
				EarliestDatestamp: metadataformats.DateStamp{Time: info.EarliestDatestamp},
			}
			for _, description := range info.Descriptions {
				resp.Identify.AddDescription(description)
			}
		case "ListMetadataFormats":
			metadataFormats := repo.ListMetadataFormats(identifier)
			if len(metadataFormats) == 0 {
				sendError(errNoMetadataFormats, "this repository supports no metadata formats", resp, w)
				return
			}
			resp.ListMetadataFormats = &listMetadataFormats{
				MetadataFormats: metadataFormats,
			}
		case "ListSets":
			sets, completeListSize, err := repo.ListSets(offset)
			if err != nil {
				panic(err)
			}
			if len(sets) == 0 {
				sendError(errNoSetHierarchy, "this repository does not support sets", resp, w)
				return
			}
			resp.ListSets = &listSets{Sets: sets}
			if uint(len(sets)) == info.MaxPageSize {
				resumptionToken.offset += info.MaxPageSize
				if err := resumptionToken.Encode(); err != nil {
					panic(err)
				}
				resumptionToken.CompleteListSize = completeListSize
				resp.ListSets.ResumptionToken = &resumptionToken.ResumptionToken
			}
		case "GetRecord":
			if identifier == nil || metadataPrefix == "" {
				sendError(errBadArgument, "missing required parameters 'identifier' and 'metadataPrefix'", resp, w)
				return
			}
			if err = checkMetadataPrefix(metadataPrefix, repo, identifier); err != nil {
				sendError(errCannotDisseminateFormat, "this repository does not support given metadata format", resp, w)
				return
			}
			record, err := repo.GetRecord(*identifier, metadataPrefix)
			if err != nil {
				sendError(errBadArgument, escapeXML(err.Error()), resp, w)
				return
			}
			if record == nil {
				sendError(errIdDoesNotExist, "no record with given identifier", resp, w)
				return
			}
			resp.GetRecord = &getRecord{
				Record: *record,
			}
			resp.setupSchemaLocations(metadataPrefix)
		case "ListIdentifiers":
			if metadataPrefix == "" {
				sendError(errBadArgument, "missing required parameter 'metadataPrefix'", resp, w)
				return
			}
			if err = checkMetadataPrefix(metadataPrefix, repo, nil); err != nil {
				sendError(errCannotDisseminateFormat, "this repository does not support given metadata format", resp, w)
				return
			}
			identifiers, completeListSize, err := repo.ListIdentifiers(metadataPrefix, set, from, until, offset)
			if err != nil {
				panic(err)
			}
			if len(identifiers) == 0 {
				sendError(errNoRecordsMatch, "the request produced no results", resp, w)
				return
			}
			resp.ListIdentifiers = &listIdentifiers{Headers: identifiers}
			if uint(len(identifiers)) == info.MaxPageSize {
				resumptionToken.offset += info.MaxPageSize
				if err := resumptionToken.Encode(); err != nil {
					panic(err)
				}
				resumptionToken.CompleteListSize = completeListSize
				resp.ListIdentifiers.ResumptionToken = &resumptionToken.ResumptionToken
			}
		default:
			sendError(errBadVerb, "illegal OAI-PMH verb", resp, w)
			return
		}

		resp.Request.Verb = verb
		resp.Request.MetadataPrefix = metadataPrefix
		if set != nil {
			resp.Request.Set = *set
		}
		if identifier != nil {
			resp.Request.Identifier = *identifier
		}
		send(resp, w)
	}
}

func checkMetadataPrefix(metadataPrefix string, repo Repository, identifier *string) error {
	for _, format := range repo.ListMetadataFormats(identifier) {
		if format.MetadataPrefix == metadataPrefix {
			return nil
		}
	}
	return errors.New("unknown metadataPrefix")
}

func baseURL(info RepositoryInfo, r *http.Request) string {
	url := info.BaseURL
	if url != nil {
		return *url
	}
	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	return fmt.Sprintf("%s://%s%s", scheme, r.Host, r.URL.Path)
}

func escapeXML(s string) string {
	var b bytes.Buffer
	xml.EscapeText(&b, []byte(s))
	return b.String()
}

func sendError(code string, message string, resp response, w http.ResponseWriter) {
	resp.Error = &oaipmhError{
		Code: code,
		Body: message,
	}
	send(resp, w)
}

func send(resp response, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/xml")
	if _, err := w.Write([]byte(xml.Header)); err != nil {
		panic(err)
	}
	if err := xml.NewEncoder(w).Encode(resp); err != nil {
		panic(err)
	}
}
