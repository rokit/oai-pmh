package oaipmh

import (
	"encoding/xml"
	"fmt"
	"time"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/mets"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/mods"
	oaidc "gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/oai-dc"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type response struct {
	XMLName             xml.Name                  `xml:"http://www.openarchives.org/OAI/2.0/ OAI-PMH"`
	XMLXSI              string                    `xml:"xmlns:xsi,attr"`
	XSISchemaLocation   string                    `xml:"xsi:schemaLocation,attr"`
	DCPrefix            string                    `xml:"xmlns:dc,attr,omitempty"`
	XlinkPrefix         string                    `xml:"xmlns:xlink,attr,omitempty"`
	MetsPrefix          string                    `xml:"xmlns:mets,attr,omitempty"`
	ModsPrefix          string                    `xml:"xmlns:mods,attr,omitempty"`
	ResponseDate        metadataformats.DateStamp `xml:"responseDate"`
	Request             request
	Error               *oaipmhError
	Identify            *identify            `xml:",omitempty"`
	ListMetadataFormats *listMetadataFormats `xml:",omitempty"`
	ListSets            *listSets            `xml:",omitempty"`
	GetRecord           *getRecord           `xml:",omitempty"`
	ListIdentifiers     *listIdentifiers     `xml:",omitempty"`
	ListRecords         *listRecords         `xml:",omitempty"`
}

func (r *response) setupSchemaLocations(metadataPrefix string) {
	if metadataPrefix == oaidc.PREFIX {
		r.XSISchemaLocation += fmt.Sprintf(" %s %s http://purl.org/dc/elements/1.1/ http://dublincore.org/schemas/xmls/simpledc20021212.xsd", oaidc.NAMESPACE, oaidc.SCHEMA_LINK)
		r.DCPrefix = "http://purl.org/dc/elements/1.1/"
	} else {
		if metadataPrefix == mods.PREFIX || metadataPrefix == mets.PREFIX {
			r.XSISchemaLocation += fmt.Sprintf(" %s %s %s %s", mods.NAMESPACE, mods.SCHEMA_LINK, xlink.NAMESPACE, xlink.SCHEMA_LINK)
			r.XlinkPrefix = xlink.NAMESPACE
			r.ModsPrefix = mods.NAMESPACE
		}
		if metadataPrefix == mets.PREFIX {
			r.XSISchemaLocation += fmt.Sprintf(" %s %s", mets.NAMESPACE, mets.SCHEMA_LINK)
			r.MetsPrefix = mets.PREFIX
		}
	}
}

func newResponse(baseURL string) response {
	return response{
		XMLXSI:            "http://www.w3.org/2001/XMLSchema-instance",
		XSISchemaLocation: "http://www.openarchives.org/OAI/2.0/ https://www.openarchives.org/OAI/2.0/OAI-PMH.xsd",
		ResponseDate:      metadataformats.DateStamp{Time: time.Now()},
		Request: request{
			BaseURL: baseURL,
		},
	}
}
