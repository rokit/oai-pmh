package oaipmh

import (
	"encoding/xml"
	"time"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
)

type request struct {
	XMLName         xml.Name                   `xml:"request"`
	BaseURL         string                     `xml:",chardata"`
	Verb            string                     `xml:"verb,attr,omitempty"`
	From            *metadataformats.DateStamp `xml:"from,attr,omitempty"`
	Until           *metadataformats.DateStamp `xml:"until,attr,omitempty"`
	MetadataPrefix  string                     `xml:"metadataPrefix,attr,omitempty"`
	Set             string                     `xml:"set,attr,omitempty"`
	Identifier      string                     `xml:"identifier,attr,omitempty"`
	ResumptionToken string                     `xml:"resumptionToken,attr,omitempty"`
}

func (r *request) withVerb(verb string) *request {
	r.Verb = verb
	return r
}

func (r *request) withMetadataPrefix(metadataPrefix string) *request {
	r.MetadataPrefix = metadataPrefix
	return r
}

func (r *request) withSet(set string) *request {
	r.Set = set
	return r
}

func (r *request) withFrom(date *time.Time) *request {
	if date != nil {
		r.From = &metadataformats.DateStamp{Time: *date}
	}
	return r
}

func (r *request) withUntil(date *time.Time) *request {
	if date != nil {
		r.Until = &metadataformats.DateStamp{Time: *date}
	}
	return r
}

func (r *request) withIdentifier(identifier *string) *request {
	if identifier != nil {
		r.Identifier = *identifier
	}
	return r
}
